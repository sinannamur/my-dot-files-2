let mapleader=" "

" Use system clipboard
set clipboard+=unnamedplus

source $HOME/.config/nvim/vim-plug/plugins.vim

" Basics

	syntax on
	set encoding=utf-8
	set number relativenumber
	set mouse=a
	set ignorecase
	set cursorline
	highlight CursorLineNr     ctermfg=7    ctermbg=8       cterm=none
	highlight CursorLine     ctermfg=7    ctermbg=8       cterm=none



" Spellcheck
autocmd FileType tex,latex,markdown setlocal spell spelllang=en_gb

" Autocomplete
" set complete+=kspell

" Vertically center document when entering insert mode
autocmd InsertEnter * norm zz

" Remove trailing whitespace on save
autocmd BufWritePre * %s/\s\+$//e

" Enable and disable auto comment
map <leader>c :setlocal formatoptions-=cro<CR>
map <leader>C :setlocal formatoptions=cro<CR>

" Enable spell checking, s for spell check
map <leader>s :setlocal spell! spelllang=tr<CR>

" Enable Disable Auto Indent
map <leader>i :setlocal autoindent<CR>
map <leader>I :setlocal noautoindent<CR>

" Shell check
map <leader>p :set spell!<CR>

" Alias write  nd quit to Q
nnoremap <leader>q :wq<CR>
nnoremap <leader>w :w<CR>

map <C-n> :NERDTreeToggle<CR>

" Plugin
call plug#begin()
Plug 'vimwiki/vimwiki'
Plug 'itchyny/lightline.vim'
Plug 'kovetskiy/sxhkd-vim'                         " sxhkd highlighting
Plug 'vim-python/python-syntax'                    " Python highlighting
"{{ File management }}
    Plug 'vifm/vifm.vim'                               " Vifm
    Plug 'scrooloose/nerdtree'                         " Nerdtree
    Plug 'tiagofumo/vim-nerdtree-syntax-highlight'     " Highlighting Nerdtree
    Plug 'ryanoasis/vim-devicons'                      " Icons for Nerdtree
call plug#end()

