#! /bin/sh
picom &
nitrogen --restore &
sxhkd &
nm-applet &
xinput --set-prop 14 'libinput Accel Speed' -0.40 &
tail -f /tmp/xobpipe | xob &
python ~/.scripts/xob-volume.py | xob &
