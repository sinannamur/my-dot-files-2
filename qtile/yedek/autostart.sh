#! /bin/sh
picom &
nitrogen --restore &
sxhkd &
nm-applet &
xinput --set-prop 14 'libinput Accel Speed' -0.40 &
